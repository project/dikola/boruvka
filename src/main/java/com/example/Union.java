package com.example;

public class Union {

    private int[] parents;
    private int[] ranks;

    public Union(int n) {
        parents = new int[n];
        ranks = new int[n];

        for (int i = 0; i < n; i++) {
            parents[i] = i;
            ranks[i] = 0;
        }
    }

    public int findParent(int u) {
        while (u != parents[u])
            u = parents[u];

        return u;
    }

    public void union(int u, int v) {
        int uParent = findParent(u);
        int vParent = findParent(v);

        if (uParent == vParent)
            return;


        if (ranks[uParent] < ranks[vParent]) {
            parents[uParent] = vParent;
        } else if (ranks[uParent] > ranks[vParent]) {
            parents[vParent] = uParent;
        } else {
            parents[vParent] = uParent;
            ranks[uParent]++;
        }

    }

}
