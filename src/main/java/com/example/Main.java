package com.example;

import com.google.common.graph.EndpointPair;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;

public class Main {

    private static MutableValueGraph<Integer, Integer> mst = ValueGraphBuilder.undirected().build();
    private static int totalWeight;

    public Main(MutableValueGraph<Integer, Integer> graph) {

        int size = graph.nodes().size();
        Union uf = new Union(size);

        // Повторяется до log N раз пока не останется N-1 граней
        for (int i = 1; i < size && mst.edges().size() < size - 1; i = i + i) {

            EndpointPair<Integer>[] closestEdgeArray = new EndpointPair[size];

            FindShortestWay(graph, uf, closestEdgeArray);

            MakeMST(graph, size, uf, closestEdgeArray);
        }
    }

    private void FindShortestWay(MutableValueGraph<Integer, Integer> graph, Union uf, EndpointPair<Integer>[] closestEdgeArray) {
        // Поиск наикратчайшего пути
        for (EndpointPair<Integer> edge : graph.edges()) {
            int u = edge.nodeU();
            int v = edge.nodeV();
            int uParent = uf.findParent(u);
            int vParent = uf.findParent(v);

            if (uParent == vParent)
                continue;

            int weight = graph.edgeValueOrDefault(u, v, 0);

            if (closestEdgeArray[uParent] == null)
                closestEdgeArray[uParent] = edge;

            if (closestEdgeArray[vParent] == null)
                closestEdgeArray[vParent] = edge;


            int uParentWeight = graph.edgeValueOrDefault(closestEdgeArray[uParent].nodeU(), closestEdgeArray[uParent].nodeV(), 0);
            int vParentWeight = graph.edgeValueOrDefault(closestEdgeArray[vParent].nodeU(), closestEdgeArray[vParent].nodeV(), 0);

            if (weight < uParentWeight)
                closestEdgeArray[uParent] = edge;

            if (weight < vParentWeight)
                closestEdgeArray[vParent] = edge;

        }
    }

    private void MakeMST(MutableValueGraph<Integer, Integer> graph, int size, Union uf, EndpointPair<Integer>[] closestEdgeArray) {
        // Добавление грани к MST
        for (int i = 0; i < size; i++) {
            EndpointPair<Integer> edge = closestEdgeArray[i];

            if (edge != null) {
                int u = edge.nodeU();
                int v = edge.nodeV();
                int weight = graph.edgeValueOrDefault(u, v, 0);

                // Проверка на то чтобы не добавить дублирующую грань
                if (uf.findParent(u) != uf.findParent(v)) {
                    mst.putEdgeValue(u, v, weight);
                    totalWeight += weight;
                    uf.union(u, v);
                }
            }
        }
    }

    public int getTotalWeight() {
        return totalWeight;
    }

    public String toString() {
        return "Грани: " + mst.toString().split("edges:")[1] + " | Вес: " + totalWeight;
    }

}
