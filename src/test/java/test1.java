import com.example.Main;
import com.google.common.graph.MutableValueGraph;
import com.google.common.graph.ValueGraphBuilder;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class test1 {

    @Test
    public void setup() {
        MutableValueGraph<Integer, Integer> graph = ValueGraphBuilder.undirected().build();
        graph.putEdgeValue(0, 1, 4);
        graph.putEdgeValue(0, 7, 8);
        graph.putEdgeValue(1, 2, 8);
        graph.putEdgeValue(1, 7, 11);
        graph.putEdgeValue(2, 3, 7);
        graph.putEdgeValue(2, 8, 2);
        graph.putEdgeValue(2, 5, 4);
        graph.putEdgeValue(3, 4, 9);
        graph.putEdgeValue(3, 5, 14);
        graph.putEdgeValue(4, 5, 10);
        graph.putEdgeValue(5, 6, 2);
        graph.putEdgeValue(6, 7, 1);
        graph.putEdgeValue(6, 8, 6);
        graph.putEdgeValue(7, 8, 8);

        Main main = new Main(graph);

        System.out.println(main.toString());

    }
}
